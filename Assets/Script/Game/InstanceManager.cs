﻿using GameInstance;
using UnityEngine;
using UnityEngine.UI;

public class InstanceManager : MonoBehaviour {

    public Sprite spritePart;
    public float partSpawnRate;
    public Transform centerPoint;
    public GameObject moveableEntity;

    public Vector2 playerPosition;
    public float moveableEntitySpeed = 0.5f;
    public GameField.EdgeBehaivour edgeType;
    public Button[] menuButtons;

    Player player;
    Vector2 position, oldPosition, playerInput;
    float counter;
    bool gameStarted = false;

    bool CanSpawn {
        get {
            counter += Time.deltaTime;
            counter = Mathf.Clamp (counter, 0, partSpawnRate);
            return counter == partSpawnRate;
        }
    }
    private void Awake () {
        GameField.Access.CreateField (new Vector2Int (50, 50), centerPoint.position, new Vector2 (0f, 0f));
        ObjectPooler.PoolObject (1, new PlayerParts (spritePart).PartReference, 30, new GameObject ("Part List").transform);
    }
    public void StartGame () {
        player = new Player ();
        Debug.Log ("Game began!");
        InvokeRepeating ("InstanceUpdate", 0, Time.fixedDeltaTime);
        position = GameField.Access.Center;
        moveableEntity.SetActive (true);
        gameStarted = true;

    }

    private void Update () {
        if (!gameStarted) return;
        playerInput = player.Position;
    }
    public void InstanceUpdate () {
        playerPosition = GameField.Access.PlayerRef.position;
        moveableEntity.transform.position = GameField.Access.MoveToPosition (ref position, oldPosition, edgeType, KillPlayer);
        oldPosition = position;
        position += playerInput * moveableEntitySpeed;
        if (CanSpawn) { GameField.Access.CreatePlayerPart (); counter = 0; }

    }

    private void KillPlayer () {
        moveableEntity.SetActive (false);
        EnableMenu ();
        CancelInvoke ("InstanceUpdate");
    }

    private void EnableMenu () {
        if (menuButtons == null) return;
        for (int i = 0; i < menuButtons.Length; i++) {
            menuButtons[i].gameObject.SetActive (true);
        }
    }
}
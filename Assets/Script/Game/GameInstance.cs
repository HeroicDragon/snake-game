﻿using System;
using System.Collections.Generic;
using System.Security.AccessControl;
using UnityEngine;

public delegate void EdgeMethod ();
namespace GameInstance {

    struct Field {

        public Vector2 position;
        public PlayerParts parts;

    }

    public struct PlayerInstance {
        public Vector2 position;
        public List<PlayerInstance> instance;
        public void AddToPlayer (PlayerParts foundPart) {

        }
    }

    public class GameField {

        Field[, ] field;

        PlayerInstance player;
        public PlayerInstance PlayerRef => player;
        Direction PlayerDirection { set; get; }

        static GameField ins;
        public static GameField Access {
            get {
                if (ins == null) {
                    ins = new GameField ();
                    ins.Awake ();
                }
                return ins;
            }
        }

        private void Awake () {
            player = new PlayerInstance ();
        }

        public Vector2Int Center {
            get {
                return new Vector2Int (field.GetLength (0) / 2, field.GetLength (1) / 2);
            }
        }

        public void CreateField (Vector2Int size, Vector2 center, Vector2 offset) {
            if (field != null) return;
            field = new Field[size.x, size.y];
            // GameObject fieldList = new GameObject ("Field");
            for (int x = 0; x < size.x; x++) {
                // GameObject orgX = new GameObject ($"Row {x}");
                // orgX.transform.parent = fieldList.transform;
                for (int y = 0; y < size.y; y++) {
                    Vector2 finalOffset = new Vector2 (offset.x * x + 1, offset.y * y + 1);
                    field[x, y].position = new Vector2 (x - (size.x / 2) + center.x + finalOffset.x, y - (size.y / 2) + center.y + finalOffset.y);
                    // VisualisePositions (x, orgX, y);

                }
            }
        }

        private void VisualisePositions (int x, GameObject parent, int y) {
            GameObject obj = GameObject.CreatePrimitive (PrimitiveType.Cube);
            obj.GetComponent<Renderer> ().material.color = Color.gray;
            obj.transform.position = field[x, y].position;
            obj.name = $"({x},{y})";
            obj.transform.parent = parent.transform;
        }

        public enum EdgeBehaivour { Loop, Stop }
        public Vector2 MoveToPosition (ref Vector2 desiredPosition, Vector2 oldPosition, EdgeBehaivour edgeType) {
            Vector2 results = Vector2.zero;
            switch (edgeType) {

                case EdgeBehaivour.Loop:
                    desiredPosition =
                        new Vector2 (Mathf.Clamp (desiredPosition.x, 0, field.GetLength (0) - 1f) %
                            field.GetLength (0), Mathf.Clamp (desiredPosition.y, 0, field.GetLength (1) - 1) % field.GetLength (1));
                    results = field[(int) desiredPosition.x, (int) desiredPosition.y].position;

                    break;

                case EdgeBehaivour.Stop:
                    desiredPosition = new Vector2 (Mathf.Clamp (desiredPosition.x, 0, field.GetLength (0) - 1), Mathf.Clamp (desiredPosition.y, 0, field.GetLength (1) - 1));
                    results = field[(int) desiredPosition.x, (int) desiredPosition.y].position;
                    break;

            }

            return results;
        }

        public Vector2 MoveToPosition (ref Vector2 desiredPosition, Vector2 oldPosition, EdgeBehaivour edgeType, EdgeMethod method) {

            Vector2 results = MoveToPosition (ref desiredPosition, oldPosition, edgeType);
            Vector2 potentialDirection = (desiredPosition - oldPosition).normalized;
            PlayerDirection = CheckDirection (potentialDirection);
            if (
                IsEqual (field.GetLength (0) - 1, desiredPosition.x) && PlayerDirection == Direction.Right ||
                IsEqual (field.GetLength (1) - 1, desiredPosition.y) && PlayerDirection == Direction.Up ||
                IsEqual (0, desiredPosition.x) && PlayerDirection == Direction.Left ||
                IsEqual (0, desiredPosition.y) && PlayerDirection == Direction.Down) {

                try {
                    method ();
                } catch (NullReferenceException) {
                    throw new NullReferenceException ("Custom Edge Method missing!");
                }
            }

            if (field[(int) desiredPosition.x, (int) desiredPosition.y].parts != null) {
                field[(int) desiredPosition.x, (int) desiredPosition.y].parts.Remove ();
            }
            player.position = field[(int) desiredPosition.x, (int) desiredPosition.y].position;
            return results;
        }

        public void CreatePlayerPart () {
            int xValue = UnityEngine.Random.Range (0, field.GetLength (0)), yValue = UnityEngine.Random.Range (0, field.GetLength (1));
            field[xValue, yValue].parts =
                (player.position != field[xValue, yValue].position) ? field[xValue, yValue].parts ?? new PlayerParts (field[xValue, yValue].position) : field[xValue, yValue].parts;
        }

        public enum Direction { Up, Right, Down, Left }
        public Direction CheckDirection (Vector2 direction) {
            if (direction == Vector2Int.up) return Direction.Up;
            if (direction == -Vector2Int.left) return Direction.Right;
            if (direction == Vector2Int.down) return Direction.Down;
            return Direction.Left;
        }
        public bool IsEqual (float value1, float value2) {
            return value1 == value2;
        }

    }

}
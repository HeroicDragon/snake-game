﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.InputSystem;

public enum ControlType { Ingame, Menu }

namespace InputManager {
    public class MenuInput : Snake.IMenuActions {

        static MenuInput ins;
        public static MenuInput Access {
            get {
                if (ins == null) {
                    ins = new MenuInput ();
                    ins.Awake ();
                }
                return ins;
            }
        }

        Snake input;
        private void Awake () {
            input = Settings.CurrentInput;
            input.Menu.SetCallbacks (this);
            Settings.AdjustActiveState (false, input, ControlType.Ingame);
            Settings.AdjustActiveState (true, input, ControlType.Menu);
        }

        public Vector2 PointerInput { get; private set; }

        public bool SelectInput { get; private set; }
        public bool OnEscapeInput { get; private set; }

        public Vector2 OnScrollInput { get; private set; }

        public void OnPointer (InputAction.CallbackContext context) {
            PointerInput = context.ReadValue<Vector2> ();
        }

        public void OnSelect (InputAction.CallbackContext context) {
            Debug.Log ("On Select Input Triggered!");
            SelectInput = (context.ReadValue<float> () == 1f) ? true : false;
        }

        public void OnExit (InputAction.CallbackContext context) {
            OnEscapeInput = context.performed;
        }

        public void OnScroll (InputAction.CallbackContext context) {
            OnScrollInput = context.ReadValue<Vector2> ();
        }
    }

    public class IngameInput : Snake.IIngameActions {

        static IngameInput ins;
        public static IngameInput Access {
            get {
                if (ins == null) {
                    ins = new IngameInput ();
                    ins.Awake ();
                }
                return ins;
            }
        }

        Snake input;
        private void Awake () {
            input = Settings.CurrentInput;
            input.Ingame.SetCallbacks (this);
            Settings.AdjustActiveState (false, input, ControlType.Menu);
            Settings.AdjustActiveState (true, input, ControlType.Ingame);

        }

        public Vector2 DirectionalInput {
            get;
            private set;
        }
        public void OnDirectionalInput (InputAction.CallbackContext context) {
            DirectionalInput = context.ReadValue<Vector2> ();
        }
    }

    public static class Settings {

        static Snake inputs;
        public static Snake CurrentInput {
            get {
                if (inputs == null) {
                    inputs = new Snake ();
                }
                return inputs;
            }

        }
        public static void AdjustActiveState (bool v, Snake input, ControlType type) {

            switch (type) {

                case ControlType.Ingame:
                    if (v) {
                        input.Ingame.Enable ();
                        return;
                    }
                    input.Ingame.Disable ();
                    return;

                case ControlType.Menu:
                    if (v) {
                        input.Menu.Enable ();
                        return;
                    }
                    input.Menu.Disable ();
                    break;
            }

        }
    }
}
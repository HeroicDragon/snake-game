using System.Collections.Generic;
using UnityEngine;
public class Player {

    Vector2Int savedPos = Vector2Int.left;
    List<PlayerParts> tailAmount = new List<PlayerParts> ();
    public Vector2Int Position {
        get {
            if (InputManager.IngameInput.Access.DirectionalInput != Vector2.zero) {
                savedPos = new Vector2Int ((int) InputManager.IngameInput.Access.DirectionalInput.x, (int) InputManager.IngameInput.Access.DirectionalInput.y);
            }
            return savedPos;
        }
    }
}
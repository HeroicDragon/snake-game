using System.Collections.Generic;
using UnityEngine;

public static class ObjectPooler {

    static Dictionary<int, List<GameObject>> dictionaryOfPools = new Dictionary<int, List<GameObject>> ();
    public static void PoolObject (int id, GameObject toBePooled, int amount, Transform parent) {
        List<GameObject> pool = new List<GameObject> ();
        for (int i = 0; i < amount; i++) {
            GameObject clone = MonoBehaviour.Instantiate (toBePooled) as GameObject;
            clone.transform.parent = parent;
            clone.transform.position -= new Vector3 (0, 0, 1);
            pool.Add (clone);
            clone.SetActive (false);
        }
        dictionaryOfPools.Add (id, pool);
        MonoBehaviour.Destroy (toBePooled);
    }

    public static GameObject InstantiatePooledObject (int id, bool isDynamic) {
        List<GameObject> foundList = dictionaryOfPools[id];
        for (int i = 0; i < foundList.Count; i++) {
            GameObject obj = foundList[i];
            if (!obj.activeInHierarchy) return foundList[i];
        }
        if (isDynamic) {
            GameObject clone = MonoBehaviour.Instantiate (foundList[0]);
            clone.transform.parent = foundList[0].transform.parent;
            dictionaryOfPools[id].Add (clone);
            return clone;
        }
        return null;
    }
}
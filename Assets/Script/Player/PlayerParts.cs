using System;
using System.Collections.Generic;
using UnityEngine;
public class PlayerParts {

    GameObject partObject;
    public GameObject PartReference { private set; get; }
    public PlayerParts (Sprite spritePart) {
        GameObject newPart = new GameObject ("Player Part");
        SpriteRenderer spriteRenderer = newPart.AddComponent<SpriteRenderer> ();
        spriteRenderer.sprite = spritePart;
        PartReference = newPart;

    }

    public PlayerParts (Vector2 position) {
        partObject = ObjectPooler.InstantiatePooledObject (1, true);
        partObject.SetActive (true);
        partObject.transform.position = position;
    }

    public PlayerParts () {

    }

    public void Remove () {
        partObject.SetActive (false);
    }
}
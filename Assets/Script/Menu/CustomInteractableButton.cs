﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CustomInteractableButton : MonoBehaviour, ISelectHandler {

    Selectable button;
    public bool debug = true;
    private void Awake () {
        button = GetComponent<Button> ();
    }

    public void OnSelect (BaseEventData eventData) {
        if (debug)
            Debug.Log ($"{button.name} is selected");
    }

    public void DebugMethod () {
        Debug.Log ("Controller input detected!");
    }

}
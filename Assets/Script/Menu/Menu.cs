﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

enum DirectionalSelection { Left, Right, Up, Down, Null }
public class Menu : MonoBehaviour {

    static Menu instance;
    public static Menu Access {
        get {
            if (instance == null) instance = GameObject.FindObjectOfType<Menu> () ?? new GameObject ("Game Manager").AddComponent<Menu> ();
            return instance;
        }
    }
    Selectable selectedButton, oldSelectedButton;

    public Selectable SetSelection { set => selectedButton = value; }

    float count;
    public float selectionRate = 0.2f;
    private void Awake () {
        selectedButton = GameObject.FindObjectOfType<Button> ();

    }
    Vector2 savedInput;

    public bool IsDelayed (float value) {
        if (oldSelectedButton != selectedButton) {
            count += Time.deltaTime;
            count = Mathf.Clamp (count, 0, value);

        }
        return count == value;
    }

    private void Update () {
        if (Gamepad.current == null || !Gamepad.current.IsActuated ()) {
            return;
        }
        //Debug.Log ($"Input detected! ->{InputManager.MenuInput.Access.SelectInput}");
        if (InputManager.MenuInput.Access.SelectInput) {
            selectedButton.GetComponent<Button> ().onClick.Invoke ();
        }
        savedInput = InputManager.MenuInput.Access.PointerInput;
        selectedButton = SelectButton () ?? selectedButton;

    }

    private Selectable SelectButton () {

        if (IsDelayed (selectionRate) || oldSelectedButton == selectedButton) {

            oldSelectedButton = selectedButton;
            count = 0;
            return selectedButton.FindSelectable (savedInput);
        }
        if (selectedButton != null) selectedButton.Select ();
        return selectedButton;

    }

    public void DebugClick () {
        Debug.Log ("Method triggered!");
    }
}